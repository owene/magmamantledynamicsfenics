"""
FEniCS test program based on a discretisation of the magma/mantle equations
with simple shear (see notes). The domain is taken to be a rectangular mesh with
periodic boundary conditions on the left and right walls. 

The initial porosity perturbation solution is a plane wave,
and the results of the computation are compared with the analytical growth rate of the perturbation
as given in Spiegelman '03.

Contributions from Sander Rhebergen (U. of Oxford), Richard Katz (U. of Oxford)
and Laura Alisic (U. of Cambridge)

Last update: 20 June 2013
"""

from dolfin import *
import sys, math, numpy

# Define Parameters
tau     = Constant(5.0/3.0)        # bulk/shear viscosity ratio
phi_0   = Constant(0.05)           # initial uniform porosity
n       = Constant(2.0)            # permeability exponent
Psi     = Constant(0.0001)         # initial amplitude of porosity perturbation
R       = Constant(1.0)            # reference compaction length
theta_0 = (5.0*pi)/12.0            # initial perturbation angle (to shear plane)
k_0     = (24.0)*pi                # initial wave number
lamb    = Constant(-27.0)          # porosity weakening factor
eta_0   = Constant(1.0)            # viscosity coefficient

# Define H & aspect
H = 1.0
aspect = 2.0
res = 100

# Create mesh and finite element
mesh = RectangleMesh(0, 0, aspect*H, H, int(aspect*res), int(res))

# Shift mesh
for x in mesh.coordinates():
  x[0] -= 0.5*aspect*H
  x[1] -= 0.5*H

# Define boundaries
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] < (-0.5 * H + DOLFIN_EPS) and on_boundary
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] > (0.5 * H - DOLFIN_EPS) and on_boundary

# Sub-domain for periodic boundary condition
class PeriodicBoundary(SubDomain):

    # Left boundary is "target domain" G
    def inside(self, x, on_boundary):
        return bool(x[0] < (-0.5*H*aspect + DOLFIN_EPS) and x[0] > (-0.5*H*aspect - DOLFIN_EPS) and on_boundary)

    # Map right boundary (H) to left boundary (G)
    def map(self, x, y):
        y[0] = x[0] - H*aspect
        y[1] = x[1]

# Create periodic boundary condition
bottom = Bottom()
top    = Top()
pbc = PeriodicBoundary()
  
# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=pbc) # velocity space 
Q = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # pressure space
W = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # porosity space
ME = MixedFunctionSpace([V,Q,W])

# Create Dirichlet BCs
topv    = Constant((0.5, 0.0))
bottomv = Constant((-0.5, 0.0))  

bc0 = DirichletBC(ME.sub(0), topv, top)             # specified velocity on top
bc1 = DirichletBC(ME.sub(0), bottomv, bottom)       # specified velocity on bottom

# Collect boundary conditions
bcs = [bc0, bc1]

# Initial condition
phi_init = Expression("phi_0 + Psi*cos(k_0*(x[0]*sin(theta_0) + x[1]*cos(theta_0)))",
                      phi_0=phi_0, Psi=Psi, k_0=k_0, theta_0=theta_0)

phi_prev = interpolate(phi_init, W)

# Define trial and test functions
dv = TrialFunction(ME)
(u, q, psi) = TestFunctions(ME)
vfunc = Function(ME)
v, p, phi = split(vfunc)

# Time-step, initial and final times
dt = 0.0001   # Time-step
t  = 0        # initial time
T  = 3        # final time

# Export to Paraview
#file = File("const_viscosity2.pvd")

# Compute solution
while (t < T):

    # Form for shear viscosity
    eta = eta_0*exp(lamb*(phi - phi_0))

    # Weak statement of equations
    L1 = 2*(eta)*inner(sym(grad(u)), sym(grad(v)))*dx + (tau*eta_0 - (2.0/3.0)*eta)*div(u)*div(v)*dx \
        - div(u)*p*dx \
        - ((R**2)/(tau + (4.0/3.0)))*inner(grad(q),((phi/0.05)**2)*grad(p))*dx \
        - q*div(v)*dx 
    L2 = (psi*(phi-phi_prev)/dt)*dx - 0.5*psi*div((1.0 - phi_prev)*v)*dx - 0.5*psi*div((1.0 - phi)*v)*dx   

    # Combine weak forms
    L = L1 + L2
    
    # Compute Jacobian
    J = derivative(L, vfunc, dv)

    # Nonlinear solver
    problem = NonlinearVariationalProblem(L, vfunc, bcs, J)
    solver  = NonlinearVariationalSolver(problem)

    # Newton solver parameters
    prm = solver.parameters
    prm['newton_solver']['absolute_tolerance'] = 1E-8
    prm['newton_solver']['relative_tolerance'] = 1E-7
    prm['newton_solver']['maximum_iterations'] = 25
    prm['newton_solver']['relaxation_parameter'] = 1.0 # between 0 & 1

    # Solve
    solver.solve()

    # Comparison of results
    k_x0      = k_0*sin(theta_0)
    k_y0      = k_0*cos(theta_0)
    k_t       = sqrt(k_x0**2 + (k_y0 - k_x0*t)**2)
    theta     = math.atan2(sin(theta_0), (cos(theta_0) - t*sin(theta_0))) 
    k_x       = k_t*sin(theta)
    k_y       = k_t*cos(theta)

    # Analytical shear band growth rate
    ds_dt_Spiegelman03 = -lamb*((k_t**2)/(k_t**2 + 1))*(1.0 - phi_0)*sin(2.0*theta) / (tau*eta_0 + 4.0/3.0) # Spiegelman '03 / Takei/Katz '12
    #ds_dt_S03 = -2.0*lamb*(1.0/(tau*eta_0 + 4.0/3.0))*(1.0 - phi_0)*(k_x*k_y) / (k_t**2 + 1.0) # Spiegelman '03
    
    #Numerical shear band growth rate
    s_dot           = Function(W)
    numerical_ds_dt = (1 - phi_0)*div(v)/(Psi)
    s_dot = project(numerical_ds_dt, W)
    ds_dt           = s_dot(0.0, 0.0)

    #Error in shear band growth rate
    ds_dt_err = (ds_dt_Spiegelman03 - ds_dt)/ds_dt_Spiegelman03
    print "ds_dt(t) (Spiegelman) = %f; numerical_ds_dt(t) = %f; rel_error = %f" \
        % (ds_dt_Spiegelman03, ds_dt, ds_dt_err)

    #file << (vfunc.split()[2])
    #plot(vfunc.split()[2])
    phi_prev.assign(vfunc.split()[2])
    t += dt
