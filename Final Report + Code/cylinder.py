"""
FEniCS program to simulate the evolution of a two-phase compactable flow around a rigid, cylindrical inclusion. 

Last update: 6 September 2013
"""
#------------------------------------------------------------------------------------------

from dolfin import *
import time, sys, math, numpy
from random import *

#parameters["num_threads"] = 6;
parameters["form_compiler"]["quadrature_degree"] = 2
parameters['form_compiler']['representation'] = 'quadrature'
parameters['form_compiler']['optimize'] = True
parameters['form_compiler']['cpp_optimize'] = True

# Define Parameters
tau     = Constant(5.0/3.0)        # bulk/shear viscosity ratio
phi_0   = Constant(0.05)           # initial uniform porosity
n       = Constant(2.0)            # permeability exponent
R       = 1.0                      # reference compaction length
lamb    = Constant(-27.0)          # porosity weakening factor
eta_0   = Constant(1.0)            # viscosity coefficient
theta_0 = (3.0*pi)/12.0            # initial perturbation angle (to shear plane)


# Mesh
r = Rectangle(0.0, 0.0, 2.0, 1.0);
c = Circle (1.0, 0.5, 0.1);
g2d = r - c;

# Generate mesh
mesh = Mesh(g2d, 90);

# Shift mesh
for x in mesh.coordinates():
  x[0] -= 1.0
  x[1] -= 0.5

#Define boundaries
bottom_str = "x[1] < (-0.5 + DOLFIN_EPS) && on_boundary"
bottom     = compile_subdomains(bottom_str)

top_str = "x[1] > (0.5 - DOLFIN_EPS) && on_boundary"
top     = compile_subdomains(top_str)

hole_str = "x[1] < ( 0.5 - sqrt(DOLFIN_EPS)) && \
            x[1] > (-0.5 + sqrt(DOLFIN_EPS)) && \
            x[0] < ( 1.0 - sqrt(DOLFIN_EPS)) && \
            x[0] > (-1.0 + sqrt(DOLFIN_EPS)) && \
            on_boundary"
hole     = compile_subdomains(hole_str)
tol = 1.0e-11

# Subdomain for periodic boundary condition (left and right boundaries)
class PeriodicBoundary(SubDomain):
    """Define periodic boundaries"""
    def __init__(self):
        SubDomain.__init__(self)

    def inside(self, x, on_boundary):
        return on_boundary and near(x[0],-0.5*2.0, 1.0e-11)

    def map(self, x, y):
        """Map slave entity to master entity"""
        y[0] = x[0] - 2.0
        y[1] = x[1]

pbc = PeriodicBoundary()

# Mark subdomains
sub_domains = FacetFunction("uint", mesh) 
top.mark(sub_domains,1)     
bottom.mark(sub_domains,2)
hole.mark(sub_domains,3)

# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=pbc) # velocity space 
Q = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # pressure space
W = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # porosity space
L = FunctionSpace(mesh, "R", 0) # Lagrange multiplier for torque

ME = MixedFunctionSpace([V,Q,W,L])

# Create Dirichlet boundary conditions at top and bottom walls
topv    = Constant((0.5, 0.0))
bottomv = Constant((-0.5, 0.0))  

bc0 = DirichletBC(ME.sub(0), topv, top)             # specified velocity on top
bc1 = DirichletBC(ME.sub(0), bottomv, bottom)       # specified velocity on bottom

# Rotation direction of inclusion should be clockwise
vc = Expression(("x[1]", "-x[0]"))  # Corrected from "(-x[1], x[0])"

# Define cylindrical boundary
bc_cylinder = DirichletBC(ME.sub(0), vc, hole)

# Collect boundary conditions
bcs = [bc0, bc1, bc_cylinder]

# Define measure for hole
ds_cylinder = ds[sub_domains](3)

# Define trial and test functions
dv = TrialFunction(ME)
(u, q, psi, lam) = TestFunctions(ME)
vfunc = Function(ME)
v, p, phi, omega = split(vfunc)

# Time-step, initial and final times
dt = 0.005      # Time-step
t  = 0          # initial time
T  = 50*dt      # final time

# random initial condition
class myrand(Expression):
      def eval(self,values,x):
         values[0] = 0.05 + gauss(-0.001, 0.001) # change this is a smaller amplitude

randf = myrand()

phi_prev = project(randf, W)

# Export to Paraview
file = File("inclusion.pvd")

# Compute solution
while (t < T):

    # Form for shear viscosity
    eta = exp(lamb*(phi - phi_0))

    # Diffusion coefficient
    m = Constant(3.0)
    epsilon_0 = Constant(0.1) 

    # Anisotropy parameters
    Theta = Constant(pi/4.0)
    alpha = Constant(0.0)
    delta=Identity(V.cell().d)

    A = as_vector([cos(Theta), sin(Theta)]) # reduced rotation matrix    
     
    
    #Define strain rate and stress tensors
    def Strain(v):
      return as_tensor((1.0/2.0)*(v[i].dx(j) + v[j].dx(i)), [i,j])

    def Visc(v):
      return as_tensor(exp(lamb*(phi - phi_0))*((tau - (2.0/3.0))*delta[i,j]*delta[k,l] + delta[i,k]*delta[j,l] +delta[i,l]*delta[j,k] - alpha*A[i]*A[j]*A[k]*A[l]), [i,j,k,l])

    def Stress(v):
      return as_tensor(Visc(v)[i,j,k,l]*Strain(v)[k,l], [i,j])

    # Define traction 
    sigma_u = Stress(u)
    sigma_v = Stress(v)
    n = FacetNormal(mesh)

    t_u = dot(sigma_u, n)
    t_v = dot(sigma_v, n)

    # Setting boundary condition on the cylinder
    v_dirichlet = v - omega*vc

    # Nitsche's method coefficient factors
    nitsche_fac = 10.0
    h_nitsche   = CellSize(mesh)

    # Crank-Nicolson scheme
    phi_CN = 0.5*phi + 0.5*phi_prev
    
    # Weak statement of equations
    L1 = inner(sym(grad(u)), Stress(v))*dx \
        - div(u)*p*dx \
        - ((R**2)/(tau + (4.0/3.0)))*inner(grad(q),((phi/0.05)**2)*grad(p))*dx \
        - q*div(v)*dx  
    L2 = (psi*(phi-phi_prev)/dt)*dx - 0.5*psi*div((1.0 - phi_prev)*v)*dx - 0.5*psi*div((1.0 - phi)*v)*dx #+ epsilon_0*inner((sqrt(inner(grad(phi_CN), grad(phi_CN)))**m)*grad(phi_CN), grad(psi))*dx  UNCOMMENTED DIFFUSION TERM


    # Nitsche's method on cylinder
    L1 += ((nitsche_fac/h_nitsche)*dot(v_dirichlet, u) \
         - dot(v_dirichlet, t_u) - dot(u, t_v))*ds_cylinder

    L1 += lam*dot(vc, t_v)*ds_cylinder

    # Combine weak forms
    L = L1 + L2
    
    # Compute Jacobian
    J = derivative(L, vfunc, dv)

    # Solve
    solve(L == 0, vfunc, bcs, J=J, solver_parameters={"newton_solver": {'absolute_tolerance': 1E-8, 'relative_tolerance': 1E-8, 'maximum_iterations': 25, 'relaxation_parameter': 1.0}})

    def traction(v):
      """Traction vector"""

      sigma = Stress(v)
      n     = FacetNormal(mesh)

      return dot(sigma, n)
    
    def torque(v):
      """Torque on an object"""

      t       = traction(v)
      x_perp  = Expression(("x[1]", "-x[0]"))

      return dot(x_perp, t)*ds_cylinder

    total_torque = assemble(torque(v))
    info("**** Torque in numerical solution = %g" % total_torque)

    file << (vfunc.split()[2])
    phi_prev.assign(vfunc.split()[2])
    t += dt

