"""
Code for automating the computation of the SVD using dolfin-adjoint. This example is for anisotropic viscosity under simple shear. 

Last update: 6 September 2013
"""

from dolfin import *
from dolfin_adjoint import *
import time, sys, math, numpy
from random import *

parameters["form_compiler"]["quadrature_degree"] = 2
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True


# Define Parameters
tau     = Constant(5.0/3.0)        # bulk/shear viscosity ratio
phi_0   = Constant(0.05)           # initial uniform porosity
n       = Constant(2.0)            # permeability exponent
R       = 1.0                      # reference compaction length
lamb    = Constant(-27.0)          # porosity weakening factor
eta_0   = Constant(1.0)            # viscosity coefficient


# Define H & aspect
H = 1.0
aspect = 2.0
res = 60

# Create mesh and finite element
mesh = RectangleMesh(0, 0, aspect*H, H, int(aspect*res), int(res))

# Shift mesh 
for x in mesh.coordinates():
  x[0] -= 0.5*aspect*H
  x[1] -= 0.5*H

# Define boundaries
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] < (-0.5 * H + DOLFIN_EPS) and on_boundary
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] > (0.5 * H - DOLFIN_EPS) and on_boundary

# Sub-domain for periodic boundary condition
class PeriodicBoundary(SubDomain):

    def inside(self, x, on_boundary):
        return bool(x[0] < (-0.5*H*aspect + DOLFIN_EPS) and x[0] > (-0.5*H*aspect - DOLFIN_EPS) and on_boundary)

    def map(self, x, y):
        y[0] = x[0] - H*aspect
        y[1] = x[1]

# Create periodic boundary condition
bottom = Bottom()
top    = Top()
pbc = PeriodicBoundary()
  
# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=pbc) # velocity space 
Q = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # pressure space
W = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # porosity space
ME = MixedFunctionSpace([V,Q,W])

# Create Dirichlet BCs
topv    = Constant((0.5, 0.0))
bottomv = Constant((-0.5, 0.0))  

bc0 = DirichletBC(ME.sub(0), topv, top)             # specified velocity on top
bc1 = DirichletBC(ME.sub(0), bottomv, bottom)       # specified velocity on bottom

# Collect boundary conditions
bcs = [bc0, bc1]

# Initial Condition
ic = Expression("phi_0",
                phi_0=phi_0)

ic = project(ic, W)

# Define trial and test functions
dv = TrialFunction(ME)
(u, q, psi) = TestFunctions(ME)

file_1 = File("GST.pvd")

def main(ic):

  # Time-step, initial and final times
  dt = 0.01   # Time-step
  t  = 0        # initial time
  T  = 10*dt       # final time

  vfunc = Function(ME)
  v, p, phi = split(vfunc)

  phi_prev = Function(W, ic, name="InitialPorosity")

# Compute solution
  while (t < T):

      # Form for shear viscosity
      eta = eta_0*exp(lamb*(phi - phi_0))
      delta=Identity(V.cell().d)

      # Anisotropy parameters
      alpha = Constant(2.0)
      Theta = Constant(pi/4.0)
   
      A = as_vector([ cos(Theta), sin(Theta)]) # reduced rotation matrix

      #Define strain rate and stress tensors
      def Strain(v):
        return as_tensor((1.0/2.0)*(v[i].dx(j) + v[j].dx(i)), [i,j])

      def Visc(v):
        return as_tensor(exp(lamb*(phi - phi_0))*((tau - (2.0/3.0))*delta[i,j]*delta[k,l] + delta[i,k]*delta[j,l] +delta[i,l]*delta[j,k] - alpha*A[i]*A[j]*A[k]*A[l]), [i,j,k,l])
      def Stress(v):
        return as_tensor(Visc(v)[i,j,k,l]*Strain(v)[k,l], [i,j])

      # Crank-Nicolson scheme
      phi_CN = 0.5*phi + 0.5*phi_prev

    
      # Weak statement of equations
      L1 = inner(sym(grad(u)), Stress(v))*dx \
          - div(u)*p*dx \
          - ((R**2)/(tau + (4.0/3.0)))*inner(grad(q),((phi/0.05)**2)*grad(p))*dx \
          - q*div(v)*dx  
      L2 = (psi*(phi-phi_prev)/dt)*dx - 0.5*psi*div((1.0 - phi_prev)*v)*dx - 0.5*psi*div((1.0 - phi)*v)*dx         

    # Combine weak forms
      L = L1 + L2
    
    # Compute Jacobian
      J = derivative(L, vfunc, dv)

    

      solve(L == 0, vfunc, bcs, J=J, solver_parameters={"newton_solver": {'absolute_tolerance': 1E-8, 'relative_tolerance': 1E-8, 'maximum_iterations': 25, 'relaxation_parameter': 1.0}})

      v, p, phi = split(vfunc)
    
      file_1 << (vfunc.split()[2])
      phi_prev = project(split(vfunc)[2], W, name="FinalPorosity")
      t += dt

  return vfunc

steps = 1

gst_in = File("gst_%s/gst_in.pvd" % steps)
gst_out = File("gst_%s/gst_out.pvd" % steps)

if MPI.process_number() == 0:
  gst_growths = file("gst_%s/gst_growths.txt" % steps, "w")

def store_gst(z, io, i):
  if io == "input":
    gst_in << (z, float(i))
    f = File("gst_%s/gst_in_%s.xml.gz" % (steps, i))
    f << z
  elif io == "output":
    gst_out << (z, float(i))
    f = File("gst_%s/gst_out_%s.xml.gz" % (steps, i))
    f << z
  else:
    raise Exception, "no idea what you're talking about"

if __name__ == "__main__":
  vfunc = main(ic)
  parameters["adjoint"]["cache_factorizations"]=True #This significantly reduces computation time, but only for low resolution and small strains
  
  gst = compute_gst("InitialPorosity", "FinalPorosity", nsv=1)
  for i in range(gst.ncv):
    (sigma, u, v, residual) = gst.get_gst(i, return_vectors=True, return_residual=True)

    print "Singular value: ", sigma
    if MPI.process_number() == 0:
      gst_growths.write("Growth rate of perturbation %s: %s\n" % (i, sigma))

    store_gst(v, "input", i)
    store_gst(u, "output", i)
