"""
FEniCS program to simulate the evolution of a two-phase aggregate in gravity driven Poiseuille flow in a 2D Cartesian geometry.
The domain has a symmetry condition at the x=0, and a no-slip condition at the left hand wall. This model includes fixed anisotropic viscosity, based on the formulation in Richard Katz & Yasuko Takei - "Consequences of viscous anisotropy in a deforming, two-phase aggregate: 1. Governing equations and linearised analysis".

Last update: 6 September 2013
"""
from dolfin import *
import time, sys, math, numpy
from random import *

parameters["form_compiler"]["quadrature_degree"] = 2
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True

# Define Parameters
tau     = Constant(5.0/3.0)        # bulk/shear viscosity ratio
phi_0   = Constant(0.05)           # initial uniform porosity
n       = Constant(2.0)            # permeability exponent
Psi     = Constant(0.01)           # initial amplitude of porosity perturbation
R       = Constant(0.1)            # reference compaction length
theta_0 = Constant(7.0*pi/12.0)    # initial perturbation angle (to shear plane)
k_0     = (24.0)*pi                # initial wave number
lamb    = Constant(-27.0)          # porosity weakening factor
eta_0   = Constant(1.0)            # viscosity coefficient
g       = Constant((0.0, -1.0))     # gra

# Define H & aspect
H = 1.0
aspect = 2.0
res = 60

# Create mesh and finite element
mesh = RectangleMesh(0, 0, H, H*aspect, int(res), int(aspect*res))

# Shift mesh
for x in mesh.coordinates():
  x[0] -= 0.5*H
  x[1] -= 0.5*H*aspect

# Define boundaries
class Left(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < (-0.5 * H + DOLFIN_EPS) and on_boundary
class Right(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > (0.5 * H - DOLFIN_EPS) and on_boundary

# Sub-domain for periodic boundary condition
class PeriodicBoundary(SubDomain):

    # Bottom boundary is "target domain" G
    def inside(self, x, on_boundary):
        return bool(x[1] < (-0.5*H*aspect + DOLFIN_EPS) and x[1] > (-0.5*H*aspect - DOLFIN_EPS) and on_boundary)

    # Map top boundary (H) to bottom boundary (G)
    def map(self, x, y):
        y[0] = x[0]  
        y[1] = x[1] - H*aspect

# Create periodic boundary condition
left = Left()
right    = Right()
pbc = PeriodicBoundary()
  
# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=pbc) # velocity space 
Q = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # pressure space
W = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # porosity space
ME = MixedFunctionSpace([V,Q,W])

# Create Dirichlet BC - no slip
rightv    = Constant((0.0, 0.0))
  bc0 = DirichletBC(ME.sub(0), rightv, right)             # no slip boundary condition at wall

# Collect boundary conditions
bcs = [bc0]

# Random initial condition
class myrand(Expression):
    def eval(self,values,x):
        values[0] = 0.05 + gauss(-0.01, 0.01)

randf = myrand()
phi_prev = project(randf, W)


# Time-step, initial and final times
dt = 0.01        # Time-step
t  = 0           # initial time
T  = 50*dt       # final time

# Define trial and test functions
dv = TrialFunction(ME)
(u, q, psi) = TestFunctions(ME)
vfunc = Function(ME)
v, p, phi = split(vfunc)

# Export to Paraview
file = File("poiseuille.pvd")


# Compute solution
while (t < T):
    
    # Form for shear viscosity
    eta = eta_0*exp(lamb*(phi - phi_0))

    # Anisotropy parameters
    delta=Identity(V.cell().d)
    alpha = Constant(2.0)
    Theta = Constant(pi/4.0)
    
    # Crank-Nicolson scheme
    phi_CN = 0.5*phi + 0.5*phi_prev
    
    
    
    A = as_vector([ cos(Theta), sin(Theta)]) # reduced rotation matrix

    # Define strain rate and stress tensors
    def Strain(v):
      return as_tensor((1.0/2.0)*(v[i].dx(j) + v[j].dx(i)), [i,j])

    def Visc(v):
      return as_tensor(exp(lamb*(phi - phi_0))*((tau - (2.0/3.0))*delta[i,j]*delta[k,l] + delta[i,k]*delta[j,l] +delta[i,l]*delta[j,k] - alpha*A[i]*A[j]*A[k]*A[l]), [i,j,k,l])
    def Stress(v):
      return as_tensor(Visc(v)[i,j,k,l]*Strain(v)[k,l], [i,j])

    L1 = inner(sym(grad(u)), Stress(v))*dx \
        - div(u)*p*dx - inner(u, g)*dx \
        - ((R**2)/(tau + (4.0/3.0)))*inner(grad(q),((phi/0.05)**2)*(grad(p) - g))*dx \
        - q*div(v)*dx - (2.0*exp(lamb*(phi - phi_0))*(v[0].dx(0))*(u[0]) + (tau - (2.0/3.0))*exp(lamb*(phi - phi_0))*div(v)*(u[0]))*ds \
        + alpha*exp(lamb*(phi - phi_0))*((0.5)*(v[0].dx(0))+ (0.5)*(v[1].dx(1)))*(u[0])*ds
    L2 = (psi*(phi-phi_prev)/dt)*dx - 0.5*psi*div((1.0 - phi_prev)*v)*dx - 0.5*psi*div((1.0 - phi)*v)*dx #+ 0.1*inner((sqrt(dot(grad(phi_CN), grad(phi_CN)))**m)*grad(phi_CN), grad(psi))*dx  UNCOMMENTED DIFFUSION TERM
      

    # Combine weak forms
    L = L1 + L2
    
    # Compute Jacobian
    J = derivative(L, vfunc, dv)

    # Solve
    solve(L == 0, vfunc, bcs, J=J, solver_parameters={"newton_solver": {'absolute_tolerance': 1E-8, 'relative_tolerance': 1E-8, 'maximum_iterations': 25, 'relaxation_parameter': 1.0}})

    #total_porosity = (vfunc.split()[2])*dx
    #E1 = assemble(total_porosity)
    #print 'E1:', E1
    
    file << (vfunc.split()[2])
    phi_prev.assign(vfunc.split()[2])
    t += dt

  
 
