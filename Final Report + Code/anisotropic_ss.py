"""
FEniCS program to simulate the evolution of a two-phase compactable flow under simple shear. This model includes fixed anisotropic viscosity, based on the formulation in Richard Katz & Yasuko Takei - "Consequences of viscous anisotropy in a deforming, two-phase aggregate: 1. Governing equations and linearised analysis".

Last update: 6 September 2013
"""
from dolfin import *
import time, sys, math, numpy
from random import *

parameters["form_compiler"]["quadrature_degree"] = 2
parameters["form_compiler"]["representation"] = "quadrature"
parameters["form_compiler"]["optimize"] = True
parameters["form_compiler"]["cpp_optimize"] = True


# Define Parameters
tau     = Constant(5.0/3.0)        # bulk/shear viscosity ratio
phi_0   = Constant(0.05)           # initial uniform porosity
n       = Constant(2.0)            # permeability exponent
Psi     = Constant(0.001)          # initial amplitude of porosity perturbation
R       = 1.0                      # reference compaction length
theta_0 = Constant(pi/4.0)         # initial perturbation angle (to shear plane)
k_0     = (8.0)*pi                 # initial wave number
lamb    = Constant(0.0)            # porosity weakening factor
eta_0   = Constant(1.0)            # viscosity coefficient


# Define H & aspect ratio
H = 1.0
aspect = 2.0
res = 60         # grid resolution


# Create mesh 
mesh = RectangleMesh(0, 0, aspect*H, H, int(aspect*res), int(res))

# Shift mesh to the centre at the origin
for x in mesh.coordinates():
  x[0] -= 0.5*aspect*H
  x[1] -= 0.5*H

# Define boundaries
class Bottom(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] < (-0.5 * H + DOLFIN_EPS) and on_boundary
class Top(SubDomain):
    def inside(self, x, on_boundary):
        return x[1] > (0.5 * H - DOLFIN_EPS) and on_boundary

# Sub-domain for periodic boundary condition
class PeriodicBoundary(SubDomain):

    
    def inside(self, x, on_boundary):
        return bool(x[0] < (-0.5*H*aspect + DOLFIN_EPS) and x[0] > (-0.5*H*aspect - DOLFIN_EPS) and on_boundary)

    
    def map(self, x, y):
        y[0] = x[0] - H*aspect
        y[1] = x[1]

# Create periodic boundary condition
bottom = Bottom()
top    = Top()
pbc = PeriodicBoundary()
  
# Define function spaces
V = VectorFunctionSpace(mesh, "CG", 2, constrained_domain=pbc) # velocity space 
Q = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # pressure space
W = FunctionSpace(mesh, "CG", 1, constrained_domain=pbc) # porosity space
ME = MixedFunctionSpace([V,Q,W])

# Create Dirichlet boundary conditions on top and bottom walls
topv    = Constant((0.5, 0.0))
bottomv = Constant((-0.5, 0.0))  

bc0 = DirichletBC(ME.sub(0), topv, top)             # specified velocity on top
bc1 = DirichletBC(ME.sub(0), bottomv, bottom)       # specified velocity on bottom

# Collect boundary conditions
bcs = [bc0, bc1]

# Initial condition

# Random initial condition
class myrand(Expression):
    def eval(self,values,x):
      values[0] = 0.05 + gauss(-0.01, 0.01)

randf = myrand()
phi_prev = project(randf, W)

#Harmonic perturbation initial condition
#ic = Expression("phi_0 + Psi*cos(k_0*(x[0]*sin(theta_0) + x[1]*cos(theta_0)))",
#                      phi_0=phi_0, Psi=Psi, k_0=k_0, theta_0=theta_0)
#phi_prev = interpolate(ic, W)

# Define trial and test functions
dv = TrialFunction(ME)
(u, q, psi) = TestFunctions(ME)
vfunc = Function(ME)
v, p, phi = split(vfunc)

# Time-step, initial and final times
dt = 0.01       # time-step
t  = 0          # initial time
T  = 50*dt      # final time

# Export to Paraview
file = File("anisotropy.pvd")

# Compute solution
while (t < T):

    # Diffusion coefficient
    m = Constant(3.0)
    epsilon_0 = Constant(0.1)
    
    # Form for shear viscosity
    eta = eta_0*exp(lamb*(phi - phi_0))

    # Anisotropy parameters
    delta=Identity(V.cell().d)
    alpha = Constant(0.0)
    Theta = Constant(pi/4.0)
     
    A = as_vector([cos(Theta), sin(Theta)]) # reduced rotation matrix

    #Define strain rate and stress tensors
    def Strain(v):
      return as_tensor((1.0/2.0)*(v[i].dx(j) + v[j].dx(i)), [i,j])

    def Stress(v):
      return as_tensor((exp(lamb*(phi - phi_0))*((tau - (2.0/3.0))*delta[i,j]*delta[k,l] + delta[i,k]*delta[j,l] +delta[i,l]*delta[j,k] - alpha*A[i]*A[j]*A[k]*A[l]))*(Strain(v)[k,l]), [i,j])

    # Crank-Nicolson scheme 
    phi_CN = 0.5*phi + 0.5*phi_prev

    # Weak statement of equations
    L1 = inner(sym(grad(u)), Stress(v))*dx \
        - div(u)*p*dx \
        - ((R**2)/(tau + (4.0/3.0)))*inner(grad(q),((phi/0.05)**2)*grad(p))*dx \
        - q*div(v)*dx  
    L2 = (psi*(phi-phi_prev)/dt)*dx - 0.5*psi*div((1.0 - phi_prev)*v)*dx - 0.5*psi*div((1.0 - phi)*v)*dx #+ epsilon_0*inner((sqrt(inner(grad(phi_CN), grad(phi_CN)))**m)*grad(phi_CN), grad(psi))*dx UNCOMMENTED DIFFUSION TERM      

    # Combine weak forms
    L = L1 + L2
    
    # Compute Jacobian
    J = derivative(L, vfunc, dv)

    # Solve
    solve(L == 0, vfunc, bcs, J=J, solver_parameters={"newton_solver": {'absolute_tolerance': 1E-8, 'relative_tolerance': 1E-8, 'maximum_iterations': 25, 'relaxation_parameter': 1.0}})
    
    # Diagnostics
    k_x0      = k_0*sin(theta_0)
    k_y0      = k_0*cos(theta_0)
    k_t       = sqrt(k_x0**2 + (k_y0 - k_x0*t)**2)
    theta     = math.atan2(sin(theta_0), (cos(theta_0) - t*sin(theta_0))) 
    k_x       = k_t*sin(theta)
    k_y       = k_t*cos(theta)

    J1 = alpha*cos(pi/4.0)*cos(pi/4.0)*(4.0*sin(pi/4.0)*sin(pi/4.0) - 1.0)
    J2 = alpha*sin(pi/4.0)*sin(pi/4.0)*(4.0*cos(pi/4.0)*cos(pi/4.0) - 1.0)
    J3 = -(alpha/4.0)*sin(pi/2.0)
    J4 = -(alpha/8.0)*sin(pi)

    D11 = (tau + (4.0/3.0))/(k_0*k_0*R*R) + tau + (4.0/3.0) - ((3.0*alpha)/(4.0))*sin(pi/2)*sin(pi/2) + J1*sin(theta)*sin(theta)*sin(theta)*sin(theta) + J2*cos(theta)*cos(theta)*cos(theta)*cos(theta) + 4.0*J3*sin(theta)*cos(theta) + 4.0*J4*cos(theta)*sin(theta)*(sin(theta)*sin(theta) - cos(theta)*cos(theta))
    D12 = J1*(sin(theta)*sin(theta)*sin(theta))*(cos(theta)) - J2*(sin(theta))*(cos(theta)*cos(theta)*cos(theta)) - J3*(sin(theta)*sin(theta) - cos(theta)*cos(theta)) - J4*(1 - 8.0*cos(theta)*cos(theta)*sin(theta)*sin(theta))
    D21 = D12
    D22 = 1.0 - (alpha/4.0)*sin(pi/2.0)*sin(pi/2.0) + (J1 + J2)*cos(theta)*cos(theta)*sin(theta)*sin(theta) - 4.0*J4*cos(theta)*sin(theta)*(sin(theta)*sin(theta) - cos(theta)*cos(theta))

    N1 = 2.0*(1.0 - (alpha/4.0)*sin(pi/2.0)*sin(pi/2.0))*sin(theta)*cos(theta)+ J3 + J4*(sin(theta)*sin(theta) - cos(theta)*cos(theta))
    N2 = -(1.0 - (alpha/4.0)*sin(pi/2.0)*sin(pi/2.0))*(sin(theta)*sin(theta) - cos(theta)*cos(theta)) + 2.0*J4*sin(theta)*cos(theta)

    # Analytical shear band growth rate
    ds_dt_Spiegelman03 = -lamb*((k_t**2)/(k_t**2 + 1))*(1.0 - phi_0)*sin(2.0*theta) / (tau*eta_0 + 4.0/3.0) # Spiegelman '03 
    ds_dt_anis = -lamb*(1.0 - phi_0)*((D22*N1 - D12*N2)/(D11*D22 - D12*D21)) # Takei/Katz '12
    
    #Numerical shear band growth rate
    s_dot           = Function(W)
    numerical_ds_dt = (1 - phi_0)*div(v)/(Psi)
    s_dot = project(numerical_ds_dt, W)
    ds_dt           = s_dot(0.0, 0.0)

    #Error in shear band growth rate
    #ds_dt_err_1 = (ds_dt_anis - ds_dt)/ds_dt_anis
    #ds_dt_err_2= (ds_dt_Spiegelman03 - ds_dt)/ds_dt_Spiegelman03
    #print "ds_dt(t) (Analytical) = %f; ds_dt(t) (Spiegelman 2003) = %f numerical_ds_dt(t) = %f; rel_error = %f" \
    #    % (ds_dt_anis, ds_dt_Spiegelman03, ds_dt, ds_dt_err)

    #check for conservation of porosity
    total_porosity = (vfunc.split()[2])*dx
    E1 = assemble(total_porosity)
    print 'E1:', E1

    v, p, phi = split(vfunc)
    file << (vfunc.split()[2])
    phi_prev.assign(vfunc.split()[2])
    t += dt


